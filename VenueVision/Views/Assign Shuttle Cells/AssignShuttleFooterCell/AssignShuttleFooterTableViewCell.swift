//
//  AssignShuttleFooterTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class AssignShuttleFooterTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var textViewCommentBox: UITextView!
    
    weak var delegate: BindAssignShuttleFooterCellToVC?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textViewCommentBox.delegate = self
    }
    
    @IBAction func tapOnSubmitBtn(_ sender: Any) {
        
    }
}

extension AssignShuttleFooterTableViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("Tells the delegate that editing of the specified text view has begun.")
        self.delegate?.didBeginEditingOnTextView()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("Tells the delegate that editing of the specified text view has ended.")
        self.delegate?.didEndEditingOnTextView()
    }
    
}
