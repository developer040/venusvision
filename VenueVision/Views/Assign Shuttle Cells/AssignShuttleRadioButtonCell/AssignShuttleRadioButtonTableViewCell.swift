//
//  AssignShuttleRadioButtonTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class AssignShuttleRadioButtonTableViewCell: UITableViewCell {

    // MARK: - OUTLETS
    @IBOutlet weak var imagePickup: UIImageView!
    @IBOutlet weak var imageDropOff: UIImageView!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func tapOnPickupBtn(_ sender: Any) {
    }
    
    @IBAction func tapOnDropOffBtn(_ sender: Any) {
    }
}
