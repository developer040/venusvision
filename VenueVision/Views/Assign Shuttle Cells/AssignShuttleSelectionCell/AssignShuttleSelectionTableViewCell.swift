//
//  AssignShuttleSelectionTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class AssignShuttleSelectionTableViewCell: UITableViewCell {

    // MARK: - OUTLETS
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewMain.addShadow()
    }
    
    // MARK: - CONFIGURE CELL
    
    func configureCell(object: AssignShuttleSelectionModel) {
        self.imageCell.image = UIImage.init(named: object.iconImage)
        self.lblTitle.text = object.title
    }
    
    
    // MARK: - SELECT ENTITY BUTTON PRESSED
    @IBAction func tapOnSelectBtn(_ sender: Any) {
        
    }
    
}
