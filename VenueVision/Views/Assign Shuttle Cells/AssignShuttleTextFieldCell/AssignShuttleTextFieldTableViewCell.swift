//
//  AssignShuttleTextFieldTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class AssignShuttleTextFieldTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var textfield: UITextField!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(object: AssignShuttleTextFieldModel) {
        self.imageIcon.image = UIImage.init(named: object.iconImage)
        self.textfield.placeholder = object.placeholderText
    }
    
}
