//
//  CustomerDetailsAccountHeaderTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 8/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class CustomerDetailsAccountHeaderTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var viewInitials: UIView!
    @IBOutlet weak var viewDetails: UIView!
    
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    
    private func setupUI() {
        self.viewInitials.cornerRadius = self.viewInitials.frame.width / 2
        self.viewDetails.addShadow()
    }
    
    // MARK: - CONFIGURE CELL
}
