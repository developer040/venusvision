//
//  ShuttleRecordContentTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 8/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ShuttleRecordContentTableViewCell: UITableViewCell {

    // MARK: - OUTLETS
    @IBOutlet weak var mainView: UIView!
    
    // MARK: - VARIABLES
    
    
    // MARK: - BOILER PLATE CODE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    
    private func setupUI() {
        self.mainView.addShadow()
    }
    
    // MARK: - CONFIGURE CELL
}
