//
//  CustomerShuttleRecordsTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 8/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class CustomerShuttleRecordsTableViewCell: UITableViewCell {

    // MARK: - OUTLETS
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - VARIABLES
    weak var delegate: BindShuttleRecordsCellToVC?
    
    // MARK: - BOILER PLATE CODE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    
    private func setupUI() {
        self.mainView.addShadow()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.ShuttleRecordContentTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.ShuttleRecordContentTableViewCell)
    }
    
    // MARK: - CONFIGURE CELL
    
    @IBAction func tapOnAddShuttleBtn(_ sender: Any) {
        self.delegate?.didTapOnAddRecordBtn()
    }
}

extension CustomerShuttleRecordsTableViewCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.ShuttleRecordContentTableViewCell, for: indexPath) as? ShuttleRecordContentTableViewCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.convertIphone6ToIphone5(size: 100.0)
    }
    
}
