//
//  MyTripsTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class MyTripsTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    
    // Bottom Views
    // Start Trip
    @IBOutlet weak var viewStartTrip: UIView!
    @IBOutlet weak var imageIconStartTrip: UIImageView!
    @IBOutlet weak var lblStartTrip: UILabel!
    // Show Route
    @IBOutlet weak var viewShowRoute: UIView!
    // Call
    @IBOutlet weak var viewCall: UIView!
    
    
    // MARK: - VARIABLES
    weak var delegate: BindMyTripCellToMyTripVC?
    
    // MARK: - BOILER PLATE CODE
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewTop.addShadow()
        self.viewBottom.addShadow()
    }
    
    
    func configureCell(isSelected: Bool) {
        if isSelected {
            self.imageIconStartTrip.image = UIImage.init(named: "start_trip_active_icon")
            self.lblStartTrip.textColor = .white
            self.viewStartTrip.backgroundColor = Constants.Colors.Base_Green_Color
        } else {
            self.imageIconStartTrip.image = UIImage.init(named: "start_trip_inactive_icon")
            self.lblStartTrip.textColor = UIColor.init(hex: 0x424242)
            self.viewStartTrip.backgroundColor = .clear
        }
    }
    
    // Bottom Views Button Actions
    @IBAction func tapOnStartTripBtn(_ sender: Any) {
        guard let sender = sender as? UIButton, let cell = sender.superview?.superview?.superview?.superview as? MyTripsTableViewCell else {
            return
        }
        self.delegate?.didTapOnStartRideBtn(at: cell)
    }
    
    @IBAction func tapOnShowRouteBtn(_ sender: Any) {
    }
    
    @IBAction func tapOnCallBtn(_ sender: Any) {
    }
    
}
