//
//  ChatCustomerSearchTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

enum SearchBtnTapped {
    case Chat
    case Shuttle
    case Default
}

class ChatCustomerSearchTableViewCell: UITableViewCell {

    // MARK: - OUTLETS
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var iconImageChat: UIImageView!
    @IBOutlet weak var lblStaticChat: UILabel!
    @IBOutlet weak var viewShuttle: UIView!
    @IBOutlet weak var lblStaticShuttle: UILabel!
    @IBOutlet weak var iconImageShuttle: UIImageView!
    
    // MARK: - VARIABLES
    weak var delegate: BindChatCellsToChatVC?
    var indexPathRow = Int()
    
    // MARK: - BOILER PLATE CODE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.buttonSelectedUnselectedUIChanges(whichBtn: .Default)
        self.viewTop.addShadow()
        self.viewBottom.addShadow()
    }
    
    func configureCell(isSelected: Bool) {
        if isSelected {
            self.buttonSelectedUnselectedUIChanges(whichBtn: .Chat)
        } else {
            self.buttonSelectedUnselectedUIChanges(whichBtn: .Default)
        }
    }
    
    // MARK: - TAP ON CHAT BTN
    @IBAction func tapOnChatBtn(_ sender: Any) {
        self.buttonSelectedUnselectedUIChanges(whichBtn: .Chat)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            self.delegate?.didTapOnSearchCellChatBtn(indexPathRow: self.indexPathRow)
        }
    }
    
    // MARK: - TAP ON SHUTTLE BUTTON
    @IBAction func tapOnShuttleBtn(_ sender: Any) {
        self.buttonSelectedUnselectedUIChanges(whichBtn: .Shuttle)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            self.delegate?.didTapOnSearchCellShuttleBtn(indexPathRow: self.indexPathRow)
        }
    }
    
    private func buttonSelectedUnselectedUIChanges(whichBtn: SearchBtnTapped) {
        switch whichBtn {
        case .Chat:
            // Chat selected
            self.viewChat.backgroundColor = Constants.Colors.Base_Green_Color
            self.lblStaticChat.textColor = .white
            self.iconImageChat.image = UIImage.init(named: "chat_icon")
            
            // Shuttle Unselected
            self.viewShuttle.backgroundColor = .clear
            self.lblStaticShuttle.textColor = UIColor.init(hex: 0x424242)
            self.iconImageShuttle.image = UIImage.init(named: "shuttle_icon")
        case .Shuttle:
            // Chat Unselected
            self.viewChat.backgroundColor = .clear
            self.lblStaticChat.textColor = UIColor.init(hex: 0x424242)
            self.iconImageChat.image = UIImage.init(named: "chat_icon")
            
            // Shuttle selected
            self.viewShuttle.backgroundColor = Constants.Colors.Base_Green_Color
            self.lblStaticShuttle.textColor = .white
            self.iconImageShuttle.image = UIImage.init(named: "shuttle_icon")
        case .Default:
            // Chat Unselected
            self.viewChat.backgroundColor = .clear
            self.lblStaticChat.textColor = UIColor.init(hex: 0x424242)
            self.iconImageChat.image = UIImage.init(named: "chat_icon")
            
            // Shuttle Unselected
            self.viewShuttle.backgroundColor = .clear
            self.lblStaticShuttle.textColor = UIColor.init(hex: 0x424242)
            self.iconImageShuttle.image = UIImage.init(named: "shuttle_icon")
            
        }
        
    }
}
