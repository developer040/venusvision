//
//  ChatCustomerListTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ChatCustomerListTableViewCell: UITableViewCell {

    @IBOutlet weak var viewMessageCount: UIView!
    @IBOutlet weak var imageGreyShuttle: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewMessageCount.layer.cornerRadius = self.viewMessageCount.frame.height / 2
        imageGreyShuttle.setOverlay(withColor: Constants.Colors.Base_Green_Color)
    }
    
    func configureCell(index: Int) {
        
        if index == 1 || index == 4 {
            imageGreyShuttle.isHidden = true
        }
        else if index % 2 == 0 {
            imageGreyShuttle.isHidden = false
            imageGreyShuttle.setOverlay(withColor: Constants.Colors.Base_Green_Color)
        } else {
            imageGreyShuttle.isHidden = false
            imageGreyShuttle.setOverlay(withColor: Constants.Colors.Base_Gray_Color)
        }
    }
    
}
