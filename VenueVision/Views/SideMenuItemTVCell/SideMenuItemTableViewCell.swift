//
//  SideMenuItemTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class SideMenuItemTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE

    override func awakeFromNib() {
        super.awakeFromNib()
//        self.contentView.backgroundColor = .clear
    }
    
    // CONFIGURE CELL
    
    func configureCell(object: SideMenuItemModel) {
        self.contentView.backgroundColor = object.isSelected ? .white : .clear
        self.lblTitle.textColor = object.isSelected ? Constants.Colors.Base_Green_Color : .white
        self.lblTitle.text = object.title
        
    }
    
}
