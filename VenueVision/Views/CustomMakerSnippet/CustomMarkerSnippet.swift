//
//  CustomMarkerSnippet.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/16/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

class CustomMarkerSnippet: UIView {

    @IBOutlet weak var imageBg: UIImageView!
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var lblTitle: UILabel!
}
