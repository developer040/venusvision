//
//  ChatCustomerSearchTableViewCell.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ChatCustomerHeaderTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var lblUnreadMsgs: UILabel!
    
    // MARK: - VARIABLES
    weak var delegate: BindChatCellsToChatVC?
    
    // MARK: - BOILER PLATE CODE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: CONFIGURE CELL
    func configureCell() {
        
    }
    
}

//
//extension ChatCustomerHeaderTableViewCell: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        print("TextField did begin editing method called")
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        print("TextField did end editing method called\(textField.text!)")
//    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        print("TextField should begin editing method called")
//        return true;
//    }
//    func textFieldShouldClear(_ textField: UITextField) -> Bool {
//        print("TextField should clear method called")
//        return true;
//    }
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        print("TextField should end editing method called")
//        return true;
//    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        print("While entering the characters this method gets called")
//        if let count = self.textFieldSearch.text?.count {
//            if count  2  {
//                self.delegate?.searchFieldHasCharacters()
//            } else if count < 1 {
//                self.delegate?.searchFieldIsEmpty()
//            }
//        } else {
//            self.delegate?.searchFieldIsEmpty()
//        }
//        return true;
//    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("TextField should return method called")
//        textField.resignFirstResponder();
//        return true;
//    }
//}
