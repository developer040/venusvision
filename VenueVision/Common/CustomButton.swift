//
//  CustomButton.swift
//  Carzaty
//
//  Created by Raza Naqvi on 07/12/2018.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

protocol CustomButtonMethods: class {
    func customize()
}
class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()// To set the button color and text size
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()// To set the button color and text size
    }
}

extension CustomButton: CustomButtonMethods {
    func customize() {
        var size : CGFloat = 0.0
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        
        switch screenHeight {
        case 480.0:
            size = 72.3/100
        case 568.0:
            size = 85.3/100
        case 667.0:
            size = 1.0
        case 736.0:
            size = 110.5/100
        case 812.0:
            size = 110.5/100
        case 896.0:
            //            size = 134.33/100
            size = 130.33/100
        default:
            size = 72.3/100
            break
        }
        
        titleLabel?.font = UIFont(name: (titleLabel?.font.fontName)!, size:((titleLabel?.font.pointSize)!)*size)
    }
}

