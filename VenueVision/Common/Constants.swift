//
//  Constants.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct Storyboards {
        static let launch = "LaunchScreen"
        static let main = "Main"
        static let login = "Login"
        static let MyTrips = "MyTrips"
        static let TripsHistory = "TripsHistory"
        static let AssignShuttle = "AssignShuttle"
        static let Chat = "Chat"
        static let Account = "Account"
        static let Shuttle = "Shuttle"
    }
    
    struct CellIdentifier {
        static let SideMenuItemTableViewCell = "SideMenuItemTableViewCell"
        static let MyTripsTableViewCell = "MyTripsTableViewCell"
        static let TripsHistoryTableViewCell = "TripsHistoryTableViewCell"
        static let AssignShuttleTextFieldTableViewCell = "AssignShuttleTextFieldTableViewCell"
        static let AssignShuttleSelectionTableViewCell = "AssignShuttleSelectionTableViewCell"
        static let AssignShuttleRadioButtonTableViewCell = "AssignShuttleRadioButtonTableViewCell"
        static let AssignShuttleHeadeTableViewCell = "AssignShuttleHeadeTableViewCell"
        static let AssignShuttleFooterTableViewCell = "AssignShuttleFooterTableViewCell"
        static let ChatCustomerListTableViewCell = "ChatCustomerListTableViewCell"
        static let ChatCustomerHeaderTableViewCell = "ChatCustomerHeaderTableViewCell"
        static let ChatCustomerSearchTableViewCell = "ChatCustomerSearchTableViewCell"
        // Customer Account Profile Cells Identifiers
        static let CustomerDetailsAccountHeaderTableViewCell = "CustomerDetailsAccountHeaderTableViewCell"
        static let CustomerNotesTableViewCell = "CustomerNotesTableViewCell"
        static let CustomerShuttleRecordsTableViewCell = "CustomerShuttleRecordsTableViewCell"
        static let ShuttleRecordContentTableViewCell = "ShuttleRecordContentTableViewCell"
    }
    
    struct UIImage {
        static let rememberMeChecked = "checked_icon"
        static let rememberMeUnchecked = "unchecked_icon"
        static let backArrow = "back_icon"
        static let sideMenu = "menu_icon"
        static let backArrowWhite = "white_back_icon"
    }
    
    struct Colors {
        static let Base_Green_Color = UIColor.init(hex: 0x8BC53F)
        static let Base_Gray_Color = UIColor.init(hex: 0x58595B)
        static let Base_Red_Color = UIColor.init(hex: 0xC01E2D)
    }
    
    struct FontName {
        static let HelveticaNeueMedium = "HelveticaNeue-Medium"
    }
    
    
    struct Keys {
        static let googleApiKey = "AIzaSyDxqBT8jht1ROfRlubQaRfRq0bjt8OT8Fs"
    }
}
