//
//  Utilities.swift
//  Carzaty
//
//  Created by Raza Naqvi on 07/12/2018.
//  Copyright © 2018 Hassan. All rights reserved.
//

import Foundation
import UIKit

enum UIUserInterfaceIdiom: Int {
    case unspecified
    case phone
    case pad
}

struct ScreenSize {
    static let screenWidth         = UIScreen.main.bounds.size.width
    static let screenHeight        = UIScreen.main.bounds.size.height
    static let screenMaxLength    = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let screenMinLength    = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

struct DeviceType {
    static let isPhone4OrLess  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength < 568.0
    static let isIPhone5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 568.0
    static let isIPhone6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 667.0
    static let isIPhone6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 736.0
    static let isIPhoneX          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 812.0
    static let isIPhoneXSMax     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 896.0
    static let isIPad              = UIDevice.current.userInterfaceIdiom == .pad
    
}

class Utilities {
    
    class func convertIphone6ToIphone5_Width(size : CGFloat) -> CGFloat {
        var tempSize = size
        let device = UIDevice.init()
        if device.userInterfaceIdiom == .phone {
            switch UIScreen.main.bounds.width {
            case 320:
                // //////print("iPhone 4 or 4S")
                tempSize = ( size * 84.5 ) / 100
            case 375:
                ////////print("iPhone 5 or 5S or 5C")
                tempSize = ( size * 100 ) / 100
            case 414:
                ////////print("iPhone 6 or 6S")
                tempSize = ( size * 110.4 ) / 100
            case 1104:
                // //////print("iPhone 6+ or 6S+")
                tempSize = ( size * 110.47 ) / 100
            default:
                tempSize = ( size * 110.47 ) / 100
            }
        } else {
            switch UIScreen.main.bounds.height {
            case 1024 :
                tempSize = (size * 125) / 100
            case 2732 :
                tempSize = (size * 187) / 100
            default: break
                //                //////print("other", terminator: "")
            }
        }
        return tempSize
    }
    class func convertIphone6ToIphone5(size: CGFloat) -> CGFloat {
        var tempSize = size
        let device = UIDevice.init()
        if device.userInterfaceIdiom == .phone {
            switch UIScreen.main.bounds.height {
            case 260:
                ////////print("iPhone Classic")
                tempSize = ( size * 72.3 ) / 100
            case 480:
                // //////print("iPhone 4 or 4S")
                tempSize = ( size * 72.3 ) / 100
            case 568:
                ////////print("iPhone 5 or 5S or 5C")
                tempSize = ( size * 84.5 ) / 100
            case 667:
                ////////print("iPhone 6 or 6S")
                tempSize = size
            case 736:
                // //////print("iPhone 6+ or 6S+")
                tempSize = ( size * 110.47 ) / 100
            case 812:
                // //////print("iPhone 6+ or 6S+")
                tempSize = ( size * 110.47 ) / 100
            case 896:
                // //////print("iPhone XR or XS Max")
                //                tempSize = ( size * 134.33 ) / 100
                tempSize = ( size * 130.33 ) / 100
            default:
                tempSize = size
                //                //////print("", terminator: "")
            }
        } else {
            switch UIScreen.main.bounds.height {
            case 1024 :
                tempSize = (size * 125) / 100
            case 2732 :
                tempSize = (size * 187) / 100
            default: break
                //                //////print("other", terminator: "")
            }
        }
        return tempSize
    }
    class func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    class func appVersion() -> String {
        guard let bundle = Bundle.main.infoDictionary else {
            return ""
        }
        let version = bundle["CFBundleShortVersionString"] as? String
        let build = bundle["CFBundleVersion"] as? String
        guard let ver = version, let bui = build else { return "" }
        return "\(ver) (\(bui))"
        //".\(b)"
    }

}
