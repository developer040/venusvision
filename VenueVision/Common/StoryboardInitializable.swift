//
//  StoryboardInitializable.swift
//  Carzaty
//
//  Created by Veer Suthar on 11/22/18.
//  Copyright © 2018 Veer Suthar. All rights reserved.
//

import UIKit

protocol StoryboardInitializable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardInitializable where Self: UIViewController {
    
    static var storyboardIdentifier: String {
        return String(describing: Self.self)
    }
    
    static func initFromStoryboard(name: String) -> Self {
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as? Self {
            return controller
        }
        fatalError("Controller didn't initialized")
    }
}
