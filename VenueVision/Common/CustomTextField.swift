//
//  CustomTextField.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/4/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

protocol CustomTextFieldMethods: class {
    func customize()
}

class CustomTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()// To set the UItextField color and text size
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()// To set the UItextField color and text size
    }
}


extension CustomTextField: CustomTextFieldMethods {
    func customize() {
        var size : CGFloat = 0.0
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        
        switch screenHeight {
        case 480.0:
            size = 72.3/100
        case 568.0:
            size = 85.3/100
        case 667.0:
            size = 1.0
        case 736.0:
            size = 110.5/100
        case 812.0:
            size = 110.5/100
        case 896.0:
            //            size = 134.33/100
            size = 130.33/100
        default:
            size = 72.3/100
            break
        }
        self.font = UIFont(name: (self.font?.fontName)!, size:((self.font?.pointSize)!)*size)
    }
}
