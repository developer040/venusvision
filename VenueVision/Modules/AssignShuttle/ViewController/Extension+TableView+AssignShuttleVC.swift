//
//  Extension+TableView+AssignShuttleVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension AssignShuttleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let object = self.dataArray[indexPath.row]
            switch object.cellType {
            case .header:
                guard let cellHeader = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.AssignShuttleHeadeTableViewCell, for: indexPath) as? AssignShuttleHeadeTableViewCell else { return UITableViewCell() }
                return cellHeader
            case .footer:
                guard let cellFooter = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.AssignShuttleFooterTableViewCell, for: indexPath) as? AssignShuttleFooterTableViewCell else { return UITableViewCell() }
                cellFooter.delegate = self
                return cellFooter
            case .textField:
                guard let cellTextField = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.AssignShuttleTextFieldTableViewCell, for: indexPath) as? AssignShuttleTextFieldTableViewCell else { return UITableViewCell() }
                cellTextField.configureCell(object: object.textFieldObject!)
                return cellTextField
            case .radio:
                guard let cellRadio = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.AssignShuttleRadioButtonTableViewCell, for: indexPath) as? AssignShuttleRadioButtonTableViewCell else { return UITableViewCell() }
                return cellRadio
            case .selection:
                guard let cellSelection = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.AssignShuttleSelectionTableViewCell, for: indexPath) as? AssignShuttleSelectionTableViewCell else { return UITableViewCell() }
                cellSelection.configureCell(object: object.selectionObject!)
                return cellSelection
            }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let object = self.dataArray[indexPath.row]
            switch object.cellType {
            case .radio:
                return Utilities.convertIphone6ToIphone5(size: 80)
            case .footer:
                return Utilities.convertIphone6ToIphone5(size: 200)
            default:
                return Utilities.convertIphone6ToIphone5(size: 60)
        }
    }
}

extension AssignShuttleViewController: BindAssignShuttleFooterCellToVC {
    func didBeginEditingOnTextView() {
//        self.view.bindToKeyboard()
    }
    
    func didEndEditingOnTextView() {
//        self.view.endEditing(true)
    }
}
