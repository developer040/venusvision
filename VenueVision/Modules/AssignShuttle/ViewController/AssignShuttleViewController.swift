//
//  AssignShuttleViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class AssignShuttleViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftBarButton: UIBarButtonItem!
    
    // MARK: - VARIABLES
    var dataArray = [AssignShuttleModel]()
    var service = AssginShuttleService()
    var isComingFromSideMenu = true
    
    // MARK: - BOILER PLATE CODE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    private func setupUI() {
        self.registerCells()
        self.setLeftBarButton()
        self.loadData()
        self.tableView.keyboardDismissMode = .onDrag
    }
    
    private func registerCells() {
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.AssignShuttleTextFieldTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.AssignShuttleTextFieldTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.AssignShuttleSelectionTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.AssignShuttleSelectionTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.AssignShuttleRadioButtonTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.AssignShuttleRadioButtonTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.AssignShuttleHeadeTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.AssignShuttleHeadeTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.AssignShuttleFooterTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.AssignShuttleFooterTableViewCell)
    }
    
    private func setLeftBarButton() {
        if isComingFromSideMenu {
            self.leftBarButton.image = UIImage.init(named: Constants.UIImage.sideMenu)
        } else {
            self.leftBarButton.image = UIImage.init(named: Constants.UIImage.backArrow)
        }
    }
    
    // MARK: - LOAD DATA
    
    private func loadData() {
        self.dataArray = self.service.assignShuttleStaticFormFields()
        self.tableView.reloadData()
    }
    
    @IBAction func tapOnLeftBarButton(_ sender: Any) {
        if isComingFromSideMenu {
            self.view.endEditing(true)
            self.sideMenuController?.revealMenu()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func tapOnSideMenuBtn(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
    
    @IBAction func tapOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

// MARK: - EXTENSION FOR STORYBOARD INITIALIZABLE

extension AssignShuttleViewController: StoryboardInitializable {}
