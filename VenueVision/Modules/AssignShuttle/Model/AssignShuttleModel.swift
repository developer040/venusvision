//
//  AssignShuttleModel.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

enum AssignShuttleCellTypeEnum {
    case header
    case footer
    case textField
    case selection
    case radio
}

enum AssignShuttleSelectionTypeEnum {
    case date
    case time
    case address
    case selectDriver
    case selectPassenger
    
}

struct AssignShuttleModel {
    let cellType: AssignShuttleCellTypeEnum
    let textFieldObject: AssignShuttleTextFieldModel?
    let selectionObject: AssignShuttleSelectionModel?
}

struct AssignShuttleTextFieldModel {
    let iconImage: String
    let placeholderText: String
}

struct AssignShuttleSelectionModel {
    let iconImage: String
    let title: String
    let type: AssignShuttleSelectionTypeEnum
}
