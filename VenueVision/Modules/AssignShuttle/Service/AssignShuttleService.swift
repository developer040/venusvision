//
//  AssignShuttleService.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

class AssginShuttleService {
    
    func assignShuttleStaticFormFields() -> [AssignShuttleModel] {
        
        var dataArray = [AssignShuttleModel]()
        
        let cellHeader = AssignShuttleModel.init(cellType: .header, textFieldObject: nil, selectionObject: nil)
        
        // Text Field Cells
        let cell1Object = AssignShuttleTextFieldModel.init(iconImage: "user_name_icon", placeholderText: "Sheila DeMarco")
        let cell1 = AssignShuttleModel.init(cellType: .textField, textFieldObject: cell1Object, selectionObject: nil)
        let cell2Object = AssignShuttleTextFieldModel.init(iconImage: "home_address_icon", placeholderText: "2999 Dufferin Street, Toronto, ON.")
        let cell2 = AssignShuttleModel.init(cellType: .textField, textFieldObject: cell2Object, selectionObject: nil)
        let cell3Object = AssignShuttleTextFieldModel.init(iconImage: "phone_num_icon", placeholderText: "1 (234) 576 - 8790")
        let cell3 = AssignShuttleModel.init(cellType: .textField, textFieldObject: cell3Object, selectionObject: nil)
        
        // Radio button Cell
        let cell4 = AssignShuttleModel.init(cellType: .radio, textFieldObject: nil, selectionObject: nil)
        
        
        // Selection Cell
        let cell5Object = AssignShuttleSelectionModel.init(iconImage: "date_icon", title: "01/07/2019", type: .date)
        let cell5 = AssignShuttleModel.init(cellType: .selection, textFieldObject: nil, selectionObject: cell5Object)
        
        let cell6Object = AssignShuttleSelectionModel.init(iconImage: "time_icon", title: "04:55 PM", type: .time)
        let cell6 = AssignShuttleModel.init(cellType: .selection, textFieldObject: nil, selectionObject: cell6Object)
        
        let cell7Object = AssignShuttleSelectionModel.init(iconImage: "address_icon", title: "Address", type: .address)
        let cell7 = AssignShuttleModel.init(cellType: .selection, textFieldObject: nil, selectionObject: cell7Object)
        
        let cell8Object = AssignShuttleSelectionModel.init(iconImage: "dropdown_arrow_icon", title: "Select Driver", type: .selectDriver)
        let cell8 = AssignShuttleModel.init(cellType: .selection, textFieldObject: nil, selectionObject: cell8Object)
        
        let cell9Object = AssignShuttleSelectionModel.init(iconImage: "dropdown_arrow_icon", title: "Passenger - 1", type: .selectPassenger)
        let cell9 = AssignShuttleModel.init(cellType: .selection, textFieldObject: nil, selectionObject: cell9Object)
        
        let cellFooter = AssignShuttleModel.init(cellType: .footer, textFieldObject: nil, selectionObject: nil)
        
        dataArray.append(cellHeader)
        dataArray.append(cell1)
        dataArray.append(cell2)
        dataArray.append(cell3)
        dataArray.append(cell4)
        dataArray.append(cell5)
        dataArray.append(cell6)
        dataArray.append(cell7)
        dataArray.append(cell8)
        dataArray.append(cell9)
        dataArray.append(cellFooter)
        
        return dataArray
        
    }
    
}
