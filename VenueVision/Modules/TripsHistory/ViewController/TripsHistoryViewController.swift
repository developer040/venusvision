//
//  TripsHistoryViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class TripsHistoryViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    private func setupUI() {
        self.registerCells()
    }
    
    private func registerCells() {
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.TripsHistoryTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.TripsHistoryTableViewCell)
    }
    
    @IBAction func tapOnSideMenuBtn(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
    
}

extension TripsHistoryViewController: StoryboardInitializable {}
