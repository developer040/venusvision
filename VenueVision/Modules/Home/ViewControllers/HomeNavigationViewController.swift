//
//  HomeNavigationViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/2/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class HomeNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension HomeNavigationViewController: StoryboardInitializable {} 
