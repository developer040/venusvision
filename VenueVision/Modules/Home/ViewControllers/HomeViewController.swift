//
//  HomeViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/2/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit
import SideMenuSwift

class HomeViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var shuttleView: UIView!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        self.navigationController?.navigationBar.isHidden = false
        let button = UIBarButtonItem()
        button.title = ""
        self.navigationItem.backBarButtonItem = button
        self.chatView.addShadow(ofColor: .darkGray, radius: 8.0, offset: .zero, opacity: 0.2)
        self.shuttleView.addShadow(ofColor: .darkGray, radius: 8.0, offset: .zero, opacity: 0.2)
    }
    
    // MARK: - CHAT BUTTON PRESS ACTION
    
    @IBAction func tapOnChatBtn(_ sender: Any) {
//        Alert.showAlert(vc: self, title: "Home Screen", message: "Chat Button Tapped")
        let chatCustomerList = ChatCustomerListViewController.initFromStoryboard(name: Constants.Storyboards.Chat)
        chatCustomerList.fromHome = true
        self.navigationController?.pushViewController(chatCustomerList, animated: true)
    }
    
    
    // MARK: - SHUTTLE BUTTON PRESS ACTION
    
    @IBAction func tapOnShuttleBtn(_ sender: Any) {
//        Alert.showAlert(vc: self, title: "Home Screen", message: "Shuttle Button Tapped")
//        let popUpVC = MyTripPopUpViewController.initFromStoryboard(name: Constants.Storyboards.MyTrips)
//        self.present(popUpVC, animated: true, completion: nil)
        
        let shuttleVC = ShuttleViewController.initFromStoryboard(name: Constants.Storyboards.Shuttle)
        shuttleVC.fromHome = true
        self.navigationController?.pushViewController(shuttleVC, animated: true)
        
    }
    
    @IBAction func tapOnSideMenuBtn(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
    
    
}

extension HomeViewController: StoryboardInitializable { }
