//
//  Extension+TableView+CustomerAccountVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 8/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension CustomerAccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.CustomerDetailsAccountHeaderTableViewCell, for: indexPath) as? CustomerDetailsAccountHeaderTableViewCell else {
                return UITableViewCell()
            }
            return headerCell
        case 1:
            guard let customerNotesCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.CustomerNotesTableViewCell, for: indexPath) as? CustomerNotesTableViewCell else {
                return UITableViewCell()
            }
            return customerNotesCell
            
        case 2:
            guard let shuttleRecordsCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.CustomerShuttleRecordsTableViewCell, for: indexPath) as? CustomerShuttleRecordsTableViewCell else {
                return UITableViewCell()
            }
            shuttleRecordsCell.delegate = self
            return shuttleRecordsCell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return Utilities.convertIphone6ToIphone5(size: 320.0)
        case 1:
            return Utilities.convertIphone6ToIphone5(size: 100.0)
        case 2:
            return Utilities.convertIphone6ToIphone5(size: 320.0)
        default:
            return 0
        }
        
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return Utilities.convertIphone6ToIphone5(size: 24.0)
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return Utilities.convertIphone6ToIphone5(size: 30.0)
//    }
    
    
}


extension CustomerAccountViewController: BindShuttleRecordsCellToVC {
    func didTapOnAddRecordBtn() {
        let vc = AssignShuttleViewController.initFromStoryboard(name: Constants.Storyboards.AssignShuttle)
        vc.isComingFromSideMenu = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
