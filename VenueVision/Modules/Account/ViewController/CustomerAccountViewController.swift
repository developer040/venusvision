//
//  CustomerAccountViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class CustomerAccountViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewInitials: UIView!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var viewCustomerNotes: UIView!
    @IBOutlet weak var backBarButton: UIBarButtonItem!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.setDefaultNavigationBar()
    }
    
    // MARK: - SETUP UI
    
    private func setupUI(){
        self.registerCells()
        self.backBarButton.image = UIImage.init(named: Constants.UIImage.backArrowWhite)
    }
    
    private func setNavigationBar() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "login_bg"), for: .default)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "edit"), style: .plain, target: self, action: #selector(tapOnInfoBtn))
    }
    
    private func setDefaultNavigationBar() {
        self.navigationController?.navigationBar.tintColor = Constants.Colors.Base_Green_Color
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : Constants.Colors.Base_Gray_Color]
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    private func registerCells(){
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.CustomerDetailsAccountHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.CustomerDetailsAccountHeaderTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.CustomerNotesTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.CustomerNotesTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.CustomerShuttleRecordsTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.CustomerShuttleRecordsTableViewCell)
    }
    
    @objc func tapOnInfoBtn() {
        Alert.showAlert(vc: self, title: "Customer Details", message: "Edit Customer Details Button")
    }
    
    @IBAction func tapOnSideMenuBtn(_ sender: Any) {
//        self.sideMenuController?.revealMenu()
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.popViewController(animated: true)

    }
}

extension CustomerAccountViewController: StoryboardInitializable {}
