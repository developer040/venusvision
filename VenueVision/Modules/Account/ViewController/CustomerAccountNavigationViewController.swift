//
//  CustomerAccountNavigationViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class CustomerAccountNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension CustomerAccountNavigationViewController: StoryboardInitializable {}
