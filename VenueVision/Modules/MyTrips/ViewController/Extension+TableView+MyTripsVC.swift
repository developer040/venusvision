//
//  Extension+TableView+MyTripsVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension MyTripsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isSelectedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.MyTripsTableViewCell, for: indexPath) as? MyTripsTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        cell.configureCell(isSelected: isSelectedArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.convertIphone6ToIphone5(size: 330)
    }
}

// extension for delegate cell implementation

extension MyTripsViewController: BindMyTripCellToMyTripVC {
    func didTapOnStartRideBtn(at cell: MyTripsTableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            Alert.showAlert(vc: self, title: "Invalid IndexPath", message: "Tap on cell not detected")
            return
        }
        
        for (index,_) in isSelectedArray.enumerated() {
            if index != indexPath.row {
                self.isSelectedArray[index] = false
            }
        }
        self.isSelectedArray[indexPath.row] = !self.isSelectedArray[indexPath.row]
        if isShowing {
            self.viewYourRideStarted.animHide(duration: 0.25)
        } else {
            self.viewYourRideStarted.animShow(duration: 0.25)
        }
        isShowing = !isShowing
        self.tableView.reloadData()
        
//        if self.startTripPreviouslyTappedOnIndexPath == nil {
//            self.startTripPreviouslyTappedOnIndexPath = indexPath
//            self.uiFunctionalityOnStartRideButton()
//
//        } else {
//            if indexPath == self.startTripPreviouslyTappedOnIndexPath {
//                self.uiFunctionalityOnStartRideButton()
//                self.tableView.reloadRows(at: [indexPath], with: .none)
//            } else {
//                self.tableView.reloadRows(at: [self.startTripPreviouslyTappedOnIndexPath!,indexPath], with: .none)
//                self.startTripPreviouslyTappedOnIndexPath = indexPath
//            }
//        }
    }

}
