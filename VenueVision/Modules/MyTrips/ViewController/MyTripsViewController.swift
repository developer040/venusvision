//
//  MyTripsViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit
import Presentr

class MyTripsViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewYourRideStarted: UIView!
    @IBOutlet weak var leftBarButton: UIBarButtonItem!
    
    // MARK: - VARIABLES
    var isSelectedArray = [false, false, false, false, false]
    var isShowing = false
    var startTripPreviouslyTappedOnIndexPath: IndexPath?
    var isComingFromSideMenu = true
    
    // MARK: - BOILER PLATE CODE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    private func setupUI() {
        self.registerCells()
        if !isComingFromSideMenu {
            self.leftBarButton.image = UIImage.init(named: "back_arrow_icon")
        } else {
            self.leftBarButton.image = UIImage.init(named: "menu_icon")
        }
    }
    
    private func registerCells() {
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.MyTripsTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.MyTripsTableViewCell)
    }
    
    @IBAction func tapOnSideMenuBtn(_ sender: Any) {
        if isComingFromSideMenu {
            self.sideMenuController?.revealMenu()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func tapOnYourRideStartedShowDetailsBtn(_ sender: Any) {
        let popUpVC = MyTripPopUpViewController.initFromStoryboard(name: Constants.Storyboards.MyTrips)
        let presenter = Presentr(presentationType: .custom(width: .fluid(percentage: 0.9), height: ModalSize.fluid(percentage: 0.3), center: .center))
        customPresentViewController(presenter, viewController: popUpVC, animated: true)
        //popUpVC.modalPresentationStyle = .overCurrentContext
        //self.present(popUpVC, animated: true, completion: nil)
    }
}

// MARK: - EXTENSION FOR STORYBOARD INITIALIZABLE
extension MyTripsViewController: StoryboardInitializable {}
