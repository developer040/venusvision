//
//  MyTripsNavigationViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class MyTripsNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension MyTripsNavigationViewController: StoryboardInitializable {}
