//
//  LoginViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    @IBOutlet weak var textFieldPassword: CustomTextField!
    @IBOutlet weak var btnRememberMe: CustomButton!
    
    // MARK: - VARIABLES
    private var isRememberMeBtnChecked: Bool = false
    
    // MARK: - BOILER PLATE CODE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        self.navigationController?.navigationBar.isHidden = true
        self.emailView.addShadow(ofColor: .darkGray, radius: 5.0, offset: .zero, opacity: 0.4)
        self.passwordView.addShadow(ofColor: .darkGray, radius: 5.0, offset: .zero, opacity: 0.4)
        UITextField.connectFields(fields: [textFieldEmail, textFieldPassword])
        //self.textFieldEmail.delegate = self
        //self.textFieldPassword.delegate = self
    }
    
    // MARK: - LOGIN BUTTON PRESS ACTION
    
    @IBAction func tapOnLogin(_ sender: Any) {
        Bootstrapper.showHome()
        AppDefaults.isUserLoggedIn = true
//        self.performSegue(withIdentifier: "fromLoginToHomeVC", sender: self)
//        let homeVC = HomeNavigationViewController.initFromStoryboard(name: Constants.Storyboards.main)
//        self.present(homeVC, animated: true, completion: nil)
    }
    
    // MARK: - REMEMBER ME BUTTON PRESS ACTION
    
    @IBAction func tapOnRememberMe(_ sender: Any) {
        if self.isRememberMeBtnChecked {
            self.isRememberMeBtnChecked = false
            let uncheckedImage = UIImage.init(named: Constants.UIImage.rememberMeUnchecked) ?? UIImage()
            self.btnRememberMe.setImage(uncheckedImage, for: .normal)
        } else {
            self.isRememberMeBtnChecked = true
            let checkedImage = UIImage.init(named: Constants.UIImage.rememberMeChecked) ?? UIImage()
            self.btnRememberMe.setImage(checkedImage, for: .normal)
        }
    }
    
    // MARK: - FORGOT PASSWORD BUTTON PRESSED
    
    @IBAction func tapOnForgotPassword(_ sender: Any) {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationItem.backBarButtonItem = backButton
    }
    
}

// MARK: - EXTENSION FOR UITEXTFIELD DELEGATE

//extension LoginViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        print("TextField did begin editing method called")
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        textField.resignFirstResponder();
//    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        print("TextField should begin editing method called")
//        return true
//    }
//    func textFieldShouldClear(_ textField: UITextField) -> Bool {
//        print("TextField should clear method called")
//        return true
//    }
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        print("TextField should end editing method called")
//        return true
//    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        print("While entering the characters this method gets called")
//        return true
//    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("TextField should return method called")
//        if textFieldEmail == textField {
//            textFieldPassword.becomeFirstResponder()
//        } else {
//            textField.resignFirstResponder()
//        }
//        return true
//
//    }
//}

// MARK: - EXTENSION FOR STORYBOARD INITILIZABLE
extension LoginViewController: StoryboardInitializable { }
