//
//  LoginNavigationViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class LoginNavigationViewController: UINavigationController {

    // MARK: - OUTLETS
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

// MARK: - EXTENSION FOR STORYBOARD INITIALIZABLE
extension LoginNavigationViewController: StoryboardInitializable {}
