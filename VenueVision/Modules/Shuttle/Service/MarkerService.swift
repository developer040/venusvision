//
//  MarkerService.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/13/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

class MarkerService {
    
    func getStaticMarkersData() -> [MarkerModel] {
        var array = [MarkerModel]()
        let position1 = MarkerModel.init(name: "ABC Dealer", snippetText: "", markerImage: "showroom_pin_icon", long: 67.053723, lat: 24.861007)
        let position2 = MarkerModel.init(name: "Shuttle 1", snippetText: "", markerImage: "shuttle_pin_icon", long: 67.054270, lat: 24.860150)
        let position3 = MarkerModel.init(name: "Shuttle 2", snippetText: "", markerImage: "shuttle_pin_icon", long: 67.052854, lat: 24.860301)
        let position4 = MarkerModel.init(name: "Emergency", snippetText: "", markerImage: "urgent_pin_icon", long: 67.053702, lat: 24.859294)
        
        
        array.append(position1)
        array.append(position2)
        array.append(position3)
        array.append(position4)
        return array
    }
}
