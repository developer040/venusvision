//
//  ShuttleViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/13/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit
import GoogleMaps

class ShuttleViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var viewDecline: UIView!
    @IBOutlet weak var viewShuttleDriverVehicle: UIView!
    @IBOutlet weak var viewDealerDetails: UIView!
    @IBOutlet weak var lblHeadingShuttleDriverVehicleView: CustomLabel!
    @IBOutlet weak var lblDealerName: CustomLabel!
    @IBOutlet weak var btnCancelDeclineView: UIButton!
    @IBOutlet weak var leftBarButton: UIBarButtonItem!
    @IBOutlet weak var viewDetailsBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewDeclineBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewShuttleDriverVehicleBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewDealerDetailsBottomConstraint: NSLayoutConstraint!
    
    // MARK: - VARIABLES
    var arrayMakers = [MarkerModel]()
    var service = MarkerService()
    var isShowing = false
    var isShowingShuttleDriverVehicle = false
    var isShowingDealerDetails = false
    var destinationMarker: GMSMarker?
    var fromHome = false
    
    // MARK: - BOILER PLATE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        self.loadData()
        self.setupBackBtnImage()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapView.animate(to: GMSCameraPosition.init(latitude: arrayMakers[0].lat, longitude: arrayMakers[0].long, zoom: 17.0))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewDeclineBottomConstraint.constant = -self.viewDecline.height+8
        self.viewDetailsBottomConstraint.constant = -self.viewDetails.height+8
        self.viewShuttleDriverVehicleBottomConstraint.constant = -self.viewShuttleDriverVehicle.height+8
        self.viewDealerDetailsBottomConstraint.constant = -self.viewDealerDetails.height+8
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        for marker in arrayMakers {
            let state_marker = GMSMarker()
            state_marker.position = CLLocationCoordinate2D(latitude: marker.lat, longitude: marker.long)
            state_marker.title = marker.name
            state_marker.snippet = nil//marker.snippetText
            state_marker.icon = UIImage.init(named: marker.markerImage)
            state_marker.map = mapView
        }
    }
    
    private func setupBackBtnImage() {
        if fromHome {
            self.leftBarButton.image = UIImage.init(named: "back_arrow_icon")
        } else {
            self.leftBarButton.image = UIImage.init(named: "menu_icon")
        }
        self.setupStaticLabelBarButtonItems()
    }
    
    private func setupStaticLabelBarButtonItems() {
        let aButton = UIButton(type: .custom)
        aButton.setTitle("Shuttle", for: .normal)
        aButton.setTitleColor(Constants.Colors.Base_Gray_Color, for: .normal)
        aButton.titleLabel?.font = UIFont.init(name: Constants.FontName.HelveticaNeueMedium, size: 18.0)
        aButton.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 50)
        aButton.isUserInteractionEnabled = false
        aButton.showsTouchWhenHighlighted = false
        let _ = UIBarButtonItem(customView: aButton)
        self.navigationItem.leftBarButtonItems = [leftBarButton] //, shuttleLabelButton]
        
        let bButton = UIButton(type: .custom)
        bButton.setTitle("Assigned Trip:", for: .normal)
        bButton.setTitleColor(Constants.Colors.Base_Gray_Color, for: .normal)
        bButton.titleLabel?.font = UIFont.init(name: Constants.FontName.HelveticaNeueMedium, size: 16.0)
        bButton.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 50)
        bButton.isUserInteractionEnabled = true
        bButton.showsTouchWhenHighlighted = false
        bButton.addTarget(self, action: #selector(tapOnRightBarButtonMoveToAssignShuttle), for: .touchUpInside)
        let assignedTripLabelButton = UIBarButtonItem(customView: bButton)
        
        let cButton = UIButton(type: .custom)
        cButton.setBackgroundImage(UIImage.init(named: "filled-circle"), for: .normal)
        cButton.setTitle("1", for: .normal)
        cButton.titleLabel?.font = UIFont.init(name: Constants.FontName.HelveticaNeueMedium, size: 16.0)
        cButton.setTitleColor(.white, for: .normal)
        cButton.frame = CGRect(x: 0.0, y: 0.0, width: 25, height: 25)
        cButton.isUserInteractionEnabled = true
        cButton.showsTouchWhenHighlighted = false
        cButton.adjustsImageWhenHighlighted = true
        cButton.addTarget(self, action: #selector(tapOnRightBarButtonMoveToAssignShuttle), for: .touchUpInside)
        let labelCountButton = UIBarButtonItem(customView: cButton)
        
        self.navigationItem.rightBarButtonItems = [labelCountButton, assignedTripLabelButton]
    }
    
    private func loadData() {
        self.arrayMakers = service.getStaticMarkersData()
    }
    
    @IBAction func tapOnSideMenuBtn(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
    
    @IBAction func tapOnBackBtn(_ sender: Any) {
        if fromHome {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.sideMenuController?.revealMenu()
        }
    }
    
    @IBAction func tapOnDeclineBtn(_ sender: Any) {
        self.viewDetails.isHidden = true
    }
    
    @IBAction func tapOnCancelDeclineView(_ sender: Any) {
        self.showHideAcceptDeclineView()
    }
    
    @IBAction func tapOnCancelShuttleDriverVehicleView(_ sender: Any) {
        self.showHideShuttleDriverVehicleView(title: "")
    }
    
    @IBAction func tapOnCancelDealerDetailsView(_ sender: Any) {
        self.showHideDealerDetailsView(title: "")
    }
    
    // MARK: - MOVE TO ASSIGN SHUTTLE SCREEN
    
    @objc private func tapOnRightBarButtonMoveToAssignShuttle() {
        let myTripsVC = MyTripsViewController.initFromStoryboard(name: Constants.Storyboards.MyTrips)
        myTripsVC.isComingFromSideMenu = false
        self.navigationController?.pushViewController(myTripsVC, animated: true)
    }
    
}

extension ShuttleViewController: StoryboardInitializable {}
