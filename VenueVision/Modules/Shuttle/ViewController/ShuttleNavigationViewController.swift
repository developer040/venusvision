//
//  ShuttleNavigationViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/13/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ShuttleNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let rootVC = self.viewControllers[0] as? ShuttleViewController {
            rootVC.fromHome = false
        }
    }
}

extension ShuttleNavigationViewController: StoryboardInitializable {}
