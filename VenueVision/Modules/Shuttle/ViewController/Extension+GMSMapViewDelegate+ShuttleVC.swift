//
//  Extension+GMSMapViewDelegate+ShuttleVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/13/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import GoogleMaps

extension ShuttleViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.mapView.animate(toLocation: marker.position)
        
        if let iconImage = marker.icon {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                if iconImage == UIImage.init(named: "shuttle_pin_icon")! {
                    self.showHideShuttleDriverVehicleView(title: marker.title ?? "")
                } else if iconImage == UIImage.init(named: "showroom_pin_icon")! {
                    self.showHideDealerDetailsView(title: marker.title ?? "")
                } else if iconImage == UIImage.init(named: "urgent_pin_icon")! {
                    self.showHideAcceptDeclineView()
                }
            })
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
    
        if marker.icon == UIImage.init(named: "showroom_pin_icon") {
            guard let view = Bundle.main.loadNibNamed("CustomMarkerSnippet", owner: self, options: nil)?.first as? CustomMarkerSnippet else {
                return UIView()
            }
            view.imageBg.image = UIImage.init(named: "black_marker")
            view.lblTitle.text = marker.title
            view.lblTitle.textColor = .white
            return view
        } else {
            guard let view = Bundle.main.loadNibNamed("CustomMarkerSnippet", owner: self, options: nil)?.first as? CustomMarkerSnippet else {
                return UIView()
            }
            view.imageBg.image = UIImage.init(named: "black_marker")
            view.imageBg.setOverlay(withColor: .white)
            view.lblTitle.text = marker.title
            view.lblTitle.textColor = .black
            return view
        }
    }
    
    func showHideAcceptDeclineView() {
        if self.isShowing {
            self.isShowing = false
            DispatchQueue.main.async {
                self.viewDetails.animHide(duration: 0.1)
                self.viewDecline.animHide(duration: 0.1)
            }
        } else {
            self.isShowing = true
            self.viewDetails.animShow(duration: 0.2)
            self.viewDecline.animShow(duration: 0.2)
            if isShowingShuttleDriverVehicle {
                self.showHideShuttleDriverVehicleView(title: "")
            }
            if isShowingDealerDetails {
                self.showHideDealerDetailsView(title: "")
            }
        }
    }
    
    func showHideShuttleDriverVehicleView(title: String) {
        if self.isShowingShuttleDriverVehicle {
            self.isShowingShuttleDriverVehicle = false
            self.viewShuttleDriverVehicle.animHide(duration: 0.1)
        } else {
            self.isShowingShuttleDriverVehicle = true
            self.lblHeadingShuttleDriverVehicleView.text = title
            DispatchQueue.main.async {
                self.viewShuttleDriverVehicle.animShow(duration: 0.2)
            }
            if isShowing {
                self.showHideAcceptDeclineView()
            }
            if isShowingDealerDetails {
                self.showHideDealerDetailsView(title: "")
            }
        }
    }
    
    func showHideDealerDetailsView(title: String) {
        if self.isShowingDealerDetails {
            self.isShowingDealerDetails = false
            self.viewDealerDetails.animHide(duration: 0.1)
        } else {
            self.isShowingDealerDetails = true
            self.lblDealerName.text = title
            self.viewDealerDetails.animShow(duration: 0.2)
            if isShowing {
                self.showHideAcceptDeclineView()
            }
            if isShowingShuttleDriverVehicle {
                self.showHideShuttleDriverVehicleView(title: "")
            }
        }
    }
}
