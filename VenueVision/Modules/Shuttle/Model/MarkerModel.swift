//
//  MarkerModel.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/13/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import CoreLocation

struct MarkerModel {
    let name: String
    let snippetText: String
    let markerImage: String
    let long: CLLocationDegrees
    let lat: CLLocationDegrees
}
