//
//  Extension+TableView+SideMenuVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arraySideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.SideMenuItemTableViewCell, for: indexPath) as? SideMenuItemTableViewCell else { return UITableViewCell() }
        cell.configureCell(object: self.arraySideMenuItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (index,_) in self.arraySideMenuItems.enumerated() {
            self.arraySideMenuItems[index].isSelected = false
        }
        self.arraySideMenuItems[indexPath.row].isSelected = true
        self.tableView.reloadData()
        self.moveToSelectedItem(indexRow:  self.arraySideMenuItems[indexPath.row].id)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utilities.convertIphone6ToIphone5(size: 60)
    }
    
    private func moveToSelectedItem(indexRow: Int) {
        
        switch indexRow {
        case 2:
            print("My Trips")
            self.sideMenuController?.hideMenu(animated: true, completion: { (success) in
                if success {
                    let mytripsNavVC = MyTripsNavigationViewController.initFromStoryboard(name: Constants.Storyboards.MyTrips)
                    self.sideMenuController?.setContentViewController(to: mytripsNavVC, animated: true, completion: nil)
                }
            })
        case 3:
            print("Trips History")
            self.sideMenuController?.hideMenu(animated: true, completion: { (success) in
                if success {
                    let tripsHistoryNavVC = TripsHistoryNavigationViewController.initFromStoryboard(name: Constants.Storyboards.TripsHistory)
                    self.sideMenuController?.setContentViewController(to: tripsHistoryNavVC, animated: true, completion: nil)
                }
            })
//        case 3:
//            print("Account")
//            self.sideMenuController?.hideMenu(animated: true, completion: { (success) in
//                if success {
//                    let customerAccountNavVC = CustomerAccountNavigationViewController.initFromStoryboard(name: Constants.Storyboards.Account)
//                    self.sideMenuController?.setContentViewController(to: customerAccountNavVC, animated: true, completion: nil)
//                }
//            })
        case 5:
            print("Assign Shuttle")
            self.sideMenuController?.hideMenu(animated: true, completion: { (success) in
                if success {
                    let assignShuttleNavVC = AssignShuttleNavigationViewController.initFromStoryboard(name: Constants.Storyboards.AssignShuttle)
                    self.sideMenuController?.setContentViewController(to: assignShuttleNavVC, animated: true, completion: nil)
                }
            })
        case 6:
            print("Chat Conversations")
            self.sideMenuController?.hideMenu(animated: true, completion: { (success) in
                if success {
                    let chatNavVC = ChatNavigationViewController.initFromStoryboard(name: Constants.Storyboards.Chat)
                    self.sideMenuController?.setContentViewController(to: chatNavVC, animated: true, completion: nil)
                }
            })
        default:
            print("Don't do anything")
            self.sideMenuController?.hideMenu(animated: true, completion: { (success) in
                if success {
                    let homeNavVC = HomeNavigationViewController.initFromStoryboard(name: Constants.Storyboards.main)
                    self.sideMenuController?.setContentViewController(to: homeNavVC, animated: true, completion: nil)
                }
            })
        }
        
    }
    
}
