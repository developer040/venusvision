//
//  SideMenuViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewInitials: UIView!
    
    // MARK: - VARIABLES
    var arraySideMenuItems = [SideMenuItemModel]()
    var service = SideMenuItemsService()
    
    // MARK: - BOILER PLATE CODE

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        self.viewInitials.cornerRadius = viewInitials.frame.size.width / 2
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        self.registerCells()
        self.loadData()
    }
    
    private func registerCells() {
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.SideMenuItemTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.SideMenuItemTableViewCell)
    }
    
    // MARK: - LOAD DATA
    
    func loadData() {
        self.arraySideMenuItems = self.service.sideMenuItemsStaticData()
        self.tableView.reloadData()
    }
    
    // MARK: - LOG OUT BUTTON PRESS ACTION
    @IBAction func tapOnLogoutBtn(_ sender: Any) {
        Alert.showConfirmationAlert(vc: self, title: "Logout", message: "Are you sure you want to logout?", actionTitle1: "Yes", actionTitle2: "No", handler1: { (_) in
            AppDefaults.isUserLoggedIn = false
            Bootstrapper.initialize()
        }) { (_) in
            print("Don't do anything")
        }
    }
}

// MARK: - EXTENSION FOR STORYBOARD INITIALIZABLE

extension SideMenuViewController: StoryboardInitializable {}
