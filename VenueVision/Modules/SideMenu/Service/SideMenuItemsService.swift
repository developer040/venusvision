//
//  SideMenuItemsService.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

class SideMenuItemsService {
    
    func sideMenuItemsStaticData() -> [SideMenuItemModel] {
        var sideMenuItems = [SideMenuItemModel]()
        
        let cell0 = SideMenuItemModel.init(id: 1, title: "Home", isSelected: true)
        let cell1 = SideMenuItemModel.init(id: 2, title: "My Trips", isSelected: false)
        let cell2 = SideMenuItemModel.init(id: 3, title: "Trips History", isSelected: false)
//        let cell3 = SideMenuItemModel.init(id: 4, title: "Account", isSelected: false)
        let cell4 = SideMenuItemModel.init(id: 5, title: "Schedule a Shuttle", isSelected: false)
        let cell5 = SideMenuItemModel.init(id: 6, title: "Chat Conversations", isSelected: false)
        
        sideMenuItems.append(cell0)
        sideMenuItems.append(cell1)
        sideMenuItems.append(cell2)
//        sideMenuItems.append(cell3)
        sideMenuItems.append(cell4)
        sideMenuItems.append(cell5)
        
        return sideMenuItems
    }
}
