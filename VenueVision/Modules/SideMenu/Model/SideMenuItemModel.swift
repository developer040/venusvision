//
//  SideMenuItemModel.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/11/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

struct SideMenuItemModel {
    let id: Int
    let title: String
    var isSelected: Bool
}
