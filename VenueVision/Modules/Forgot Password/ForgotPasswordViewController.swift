//
//  ForgotPasswordViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/2/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        self.navigationController?.navigationBar.isHidden = false
        self.emailView.addShadow(ofColor: .darkGray, radius: 8.0, offset: .zero, opacity: 0.2)
        UITextField.connectFields(fields: [textFieldEmail])
    }
    
    // MARK: - SUBMIT BUTTON PRESS ACTION
    
    @IBAction func tapOnSubmitBtn(_ sender: Any) {
        Alert.showAlert(vc: self, title: "Forgot Password", message: "Submit Button Pressed")
    }
}

//// MARK: - EXTENSION FOR TEXTFIELD DELEGATE METHODS
//extension ForgotPasswordViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        print("TextField did begin editing method called")
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        textField.resignFirstResponder();
//    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        print("TextField should begin editing method called")
//        return true
//    }
//    func textFieldShouldClear(_ textField: UITextField) -> Bool {
//        print("TextField should clear method called")
//        return true
//    }
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        print("TextField should end editing method called")
//        return true
//    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        print("While entering the characters this method gets called")
//        return true
//    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("TextField should return method called")
//        textField.resignFirstResponder()
//        return true
//
//    }
//}

// MARK: - EXTENSION FOR STORYBOARD INITIALIZABLE
extension ForgotPasswordViewController: StoryboardInitializable { }
