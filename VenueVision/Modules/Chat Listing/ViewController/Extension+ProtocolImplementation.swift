//
//  Extension+ProtocolImplementation.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension ChatCustomerListViewController: BindChatCellsToChatVC {
    func didTapOnSearchCellChatBtn(indexPathRow: Int) {
        self.dummyIsSelectedArray = Array.init(repeating: false, count: 10)
        self.dummyIsSelectedArray[indexPathRow] = true
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationItem.backBarButtonItem = backButton
        let chatMessages = ChatMessagesViewController.initFromStoryboard(name: Constants.Storyboards.Chat)
        self.navigationController?.pushViewController(chatMessages, animated: true)
    }
    
    func didTapOnSearchCellShuttleBtn(indexPathRow: Int) {
        self.dummyIsSelectedArray = Array.init(repeating: false, count: 10)
        self.dummyIsSelectedArray[indexPathRow] = true
        let assignShuttleVC = AssignShuttleViewController.initFromStoryboard(name: Constants.Storyboards.AssignShuttle)
        assignShuttleVC.isComingFromSideMenu = false
        self.navigationController?.pushViewController(assignShuttleVC, animated: true)
    }
    
}
