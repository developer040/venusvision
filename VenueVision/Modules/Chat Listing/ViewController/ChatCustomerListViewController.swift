//
//  ChatCustomerListViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ChatCustomerListViewController: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var leftBarButton: UIBarButtonItem!
    
    // MARK: - VARIABLES
    var dummyIsSelectedArray = Array.init(repeating: false, count: 10)
    var isSearching = false
    var fromHome = false
    
    // MARK: - BOILER PLATE CODE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        self.registerCells()
        self.setupBackBtnImage()
        self.searchBar.delegate = self
        self.tableView.keyboardDismissMode = .onDrag
        searchBar.backgroundImage = UIImage()
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).leftView = nil
        for subView in searchBar.subviews {
            for subView1 in subView.subviews {
                if subView1.isKind(of: UITextField.self) {
                    subView1.backgroundColor = .clear
                }
            }
        }
    }
    
    private func setupBackBtnImage() {
        if fromHome {
            self.leftBarButton.image = UIImage.init(named: "back_arrow_icon")
            self.title = "Chat"
        } else {
            self.leftBarButton.image = UIImage.init(named: "menu_icon")
            self.title = "Chat"
        }
    }
    
    private func registerCells() {
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.ChatCustomerListTableViewCell, bundle: nil
        ), forCellReuseIdentifier: Constants.CellIdentifier.ChatCustomerListTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.ChatCustomerHeaderTableViewCell, bundle: nil
        ), forCellReuseIdentifier: Constants.CellIdentifier.ChatCustomerHeaderTableViewCell)
        self.tableView.register(UINib.init(nibName: Constants.CellIdentifier.ChatCustomerSearchTableViewCell, bundle: nil
        ), forCellReuseIdentifier: Constants.CellIdentifier.ChatCustomerSearchTableViewCell)
    }
    
    @IBAction func tapOnSearchBtn(_ sender: Any) {
        
    }
    
    
    // MARK: - MENU BUTTON PRESS ACTION
    
    @IBAction func tapOnMenuBtn(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func tapOnBackBtn(_ sender: Any) {
        if fromHome {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.view.endEditing(true)
            self.sideMenuController?.revealMenu()
        }
        
    }
}

extension ChatCustomerListViewController: StoryboardInitializable {}
