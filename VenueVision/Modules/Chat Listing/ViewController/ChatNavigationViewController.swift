//
//  ChatNavigationViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class ChatNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let rootVC = self.viewControllers[0] as? ChatCustomerListViewController {
            rootVC.fromHome = false
        }
    }
}

extension ChatNavigationViewController: StoryboardInitializable {}
