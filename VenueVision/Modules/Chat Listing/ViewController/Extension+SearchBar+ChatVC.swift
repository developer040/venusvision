//
//  Extension+SearchBar+ChatVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension ChatCustomerListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.isSearching = false
            self.tableView.reloadData()
        } else {
            isSearching = true
            self.tableView.reloadData()
        }
    }
}
