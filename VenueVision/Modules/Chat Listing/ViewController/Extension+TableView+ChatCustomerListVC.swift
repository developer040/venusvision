//
//  Extension+TableView+ChatCustomerListVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit

extension ChatCustomerListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isSearching {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.ChatCustomerSearchTableViewCell, for: indexPath) as? ChatCustomerSearchTableViewCell else { return UITableViewCell() }
            cell.delegate = self
            cell.indexPathRow = indexPath.row
            cell.configureCell(isSelected: self.dummyIsSelectedArray[indexPath.row])
            return cell
        } else {
            switch indexPath.row {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.ChatCustomerHeaderTableViewCell, for: indexPath) as? ChatCustomerHeaderTableViewCell else { return UITableViewCell() }
                return cell
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.ChatCustomerListTableViewCell, for: indexPath) as? ChatCustomerListTableViewCell else { return UITableViewCell() }
                cell.configureCell(index: indexPath.row)
                return cell
            }
        }
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isSearching {
            return Utilities.convertIphone6ToIphone5(size: 220.0)
        } else {
            switch indexPath.row {
            case 0:
                return Utilities.convertIphone6ToIphone5(size: 40.0)
            default:
                return Utilities.convertIphone6ToIphone5(size: 80.0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isSearching {
            let backButton = UIBarButtonItem()
            backButton.title = ""
            self.navigationItem.backBarButtonItem = backButton
            let chatMessages = ChatMessagesViewController.initFromStoryboard(name: Constants.Storyboards.Chat)
            self.navigationController?.pushViewController(chatMessages, animated: true)
        }
    }
    
    
}
