//
//  Message.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/17/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import UIKit
import MessageKit

struct Member {
    let name: String
    let color: UIColor
}


extension Member: SenderType {
    var senderId: String {
        return name
    }
    
    var displayName: String {
        return name
    }
}

struct Message {
    let member: Member
    let text: String
    let messageId: String
}

extension Message: MessageType {
    var sender: SenderType {
        return self.member
    }
    
    var sentDate: Date {
        return Date()
    }
    
    var kind: MessageKind {
        return .text(text)
    }
}
