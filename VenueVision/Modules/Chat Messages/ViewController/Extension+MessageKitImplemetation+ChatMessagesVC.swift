//
//  Extension+MessageKitImplemetation+ChatMessagesVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/17/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation
import InputBarAccessoryView
import MessageKit

extension ChatMessagesViewController: MessagesDataSource {
    
    func currentSender() -> SenderType {
        return self.member1
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let dateString = MessageKitDateFormatter.shared.string(from: message.sentDate) 
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
}

extension ChatMessagesViewController: InputBarAccessoryViewDelegate {
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        if text != "" {
            let newMsg = Message.init(member: self.member1, text: text, messageId: "03158509751")
            self.messages.append(newMsg)
            let replyMessage = Message.init(member: self.member2, text: text, messageId: "03222702929")
            self.messages.append(replyMessage)
            messagesCollectionView.reloadData()
            messagesCollectionView.scrollToBottom()
        }
        inputBar.inputTextView.text = ""
    }
    
}

extension ChatMessagesViewController: MessageCellDelegate, MessagesLayoutDelegate, MessagesDisplayDelegate {
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        guard let dataSource = messagesCollectionView.messagesDataSource else { return .white }
        return dataSource.isFromCurrentSender(message: message) ? Constants.Colors.Base_Green_Color : UIColor(red: 230/255, green: 230/255, blue: 235/255, alpha: 1.0)
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        guard let dataSource = messagesCollectionView.messagesDataSource else { return }
        avatarView.initials = dataSource.isFromCurrentSender(message: message) ?  "MA" : "RM"
        avatarView.backgroundColor = Constants.Colors.Base_Gray_Color
    }
}
