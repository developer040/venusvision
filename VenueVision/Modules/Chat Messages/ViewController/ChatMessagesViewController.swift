//
//  ChatMessagesViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/17/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView
import IQKeyboardManagerSwift

class ChatMessagesViewController: MessagesViewController {
    
    // MARK: - OUTLETS
    
    // MARK: - VARIABLES
    var controllerTitle = "Sheila DeMarco"
    var messages: [Message] = []
    var member1: Member!
    var member2: Member!

    // MARK: - BOILER PLATE CODE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.title = self.controllerTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.messagesCollectionView.scrollToBottom(animated: false)
        }
    }
    
    // MARK: - SETUP UI
    
    private func setupUI() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "info_icon"), style: .plain, target: self, action: #selector(tapOnInfoBtn))
        self.assignDelegates()
        self.addStaticChatData()
        self.configureInputBar()
    }
    
    private func assignDelegates() {
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        //maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.delegate = self
    }
    
    private func addStaticChatData() {
        self.member1 = Member.init(name: "03158509751", color: .red)
        self.member2 = Member.init(name: "03222702929", color: .red)
        
        let mesg1 = Message.init(member: member1, text: "ABC Dealership \n\n Hello Sheila, your driver is on his way to pick up you. \nClick here to follow your driver.", messageId: "03158509751")
        let mesg2 = Message.init(member: member2, text: "Ok", messageId: "03222702929")
        self.messages.append(mesg1)
        self.messages.append(mesg2)
        self.messages.append(mesg1)
        self.messages.append(mesg2)
        self.messages.append(mesg1)
        self.messages.append(mesg2)
    }
    
    @objc func tapOnInfoBtn() {
        let accountVC = CustomerAccountViewController.initFromStoryboard(name: Constants.Storyboards.Account)
        self.navigationController?.pushViewController(accountVC, animated: true)
    }
}

// MARK: - EXTENSION FOR CONFIGURE MESSAGE INPUT BAR

extension ChatMessagesViewController {
    
    internal func configureInputBar() {
        messageInputBar.backgroundView.backgroundColor = .white
        messageInputBar.inputTextView.backgroundColor = .clear
        messageInputBar.inputTextView.layer.borderWidth = 0
        
        let items = [
            makeButton(named: "ic_library")
                .onSelected {
                    $0.tintColor = Constants.Colors.Base_Gray_Color
                    //                    let imagePicker = UIImagePickerController()
                    //                    imagePicker.delegate = self
                    //                    imagePicker.sourceType = .photoLibrary
                    //                    (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
            },
            messageInputBar.sendButton
                .configure {
                    $0.setTitle("", for: .normal)
                    $0.setImage(UIImage.init(named: "send_icon"), for: .normal)
//                    $0.setSize(CGSize(width: 50, height: 30), animated: false)
                }.onDisabled {_ in
                }.onEnabled {_ in
                }.onSelected {
                    $0.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                }.onDeselected {
                    $0.transform = CGAffineTransform.identity
            }
        ]
        
        // We can change the container insets if we want
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
        
        messageInputBar.rightStackView.axis = .horizontal
        messageInputBar.rightStackView.alignment = .fill
        messageInputBar.setStackViewItems(items, forStack: .right, animated: false)
        messageInputBar.setRightStackViewWidthConstant(to: 90, animated: false)
        messageInputBar.reloadInputViews()
    }
    
    private func makeButton(named: String) -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
//                $0.spacing = .fixed(10)
                $0.image = UIImage(named: "attachment_icon")//?.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 50, height: 30), animated: false)
            }.onSelected {
                $0.tintColor = UIColor(red: 15/255, green: 135/255, blue: 255/255, alpha: 1.0)
            }.onDeselected {
                $0.tintColor = UIColor.lightGray
            }.onTouchUpInside { _ in
                print("Item Tapped")
        }
    }
}

extension ChatMessagesViewController: StoryboardInitializable {}
