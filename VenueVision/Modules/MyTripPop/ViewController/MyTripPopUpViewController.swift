//
//  MyTripPopUpViewController.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

class MyTripPopUpViewController: UIViewController {

    // MARK: - OUTLETS
    
    // MARK: - VARIABLES
    
    // MARK: - BOILER PLATE CODE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tapOnBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - EXTENSION FOR STORYBOARD INITIALIZABLE
extension MyTripPopUpViewController: StoryboardInitializable {}
