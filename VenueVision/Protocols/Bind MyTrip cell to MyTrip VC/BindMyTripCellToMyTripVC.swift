//
//  BindMyTripCellToMyTripVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/15/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

protocol BindMyTripCellToMyTripVC: class {
    func didTapOnStartRideBtn(at cell: MyTripsTableViewCell)
}
