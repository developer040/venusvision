//
//  BindAssignShuttleFooterCellToVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/23/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

protocol BindAssignShuttleFooterCellToVC: class {
    func didBeginEditingOnTextView()
    func didEndEditingOnTextView()
}
