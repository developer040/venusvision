//
//  BindShuttleRecordsCellToVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 8/1/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

protocol BindShuttleRecordsCellToVC: class {
    func didTapOnAddRecordBtn()
}
