//
//  BindChatCellsToChatVC.swift
//  VenueVision
//
//  Created by Muhammad Raza on 7/12/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import Foundation

protocol BindChatCellsToChatVC: class {
    func didTapOnSearchCellChatBtn(indexPathRow: Int)
    func didTapOnSearchCellShuttleBtn(indexPathRow: Int)
}
