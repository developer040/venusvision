//
//  Loader.swift
//  Carzaty
//
//  Created by Raza Naqvi on 11/12/2018.
//  Copyright © 2018 Hassan. All rights reserved.
//

//import UIKit
//import MRProgress
//import SVProgressHUD
//
//class Loader {
//    public static func showLoader(title: String = "Please wait...", viewController: UIViewController){
////        MRProgressOverlayView.showOverlayAdded(to: viewController.view, title: title, mode: .indeterminateSmall, animated: true)
//        
//        SVProgressHUD.setMaximumDismissTimeInterval(TimeInterval.init(100.0))
//        SVProgressHUD.setImageViewSize(CGSize.init(width: 110, height: 50))
//        SVProgressHUD.setShouldTintImages(false)
//        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.gradient)
//        SVProgressHUD.show(UIImage.init(named: "car-loader") ?? UIImage(), status: "Loading..")
//    }
//
//    public static func dismissLoader(viewController: UIViewController){
//        SVProgressHUD.dismiss()
////        MRProgressOverlayView.dismissAllOverlays(for: viewController.view, animated: true)
//    }
//}
