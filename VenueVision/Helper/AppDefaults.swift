//
//  AppDefaults.swift
//  Carzaty
//
//  Created by Raza Naqvi on 04/12/2018.
//  Copyright © 2018 Hassan. All rights reserved.
//

import Foundation

class AppDefaults {
    
    public static let defaults = UserDefaults.init()
    
    // MARK: - CLEAR ALL USER DEFAULTS
    
    public static func clearUserDefaults(){
        let dictionary = AppDefaults.defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            
            //FIXME: I omit deleting of persisted language by user. 
            if (key == "AppleLanguages" ||
                key == "hasUserSelectedAnyLanguage") {
                //Don't Remove These two keys
            }else {
                defaults.removeObject(forKey: key)
            }
        }
        
        let dictionary2 = AppDefaults.defaults.dictionaryRepresentation()
        print("Key is \(dictionary2)")
    }
    
    
    // MARK: - API TOKEN
    
    public static var apiToken: String {
        get{
            if let token = AppDefaults.defaults.string(forKey: "apiToken") {
                return token
            }else{
                return ""
            }
        }
        set{
            AppDefaults.defaults.set(newValue, forKey: "apiToken")
            
        }
    }
    
    
    // MARK: - FCM TOKEN
    
    public static var fcmToken: String {
        
        get{
            if let token = AppDefaults.defaults.string(forKey: "FCMToken") {
                return token
            }else{
                return ""
            }
        }
        set{
            AppDefaults.defaults.set(newValue, forKey: "FCMToken")
        }
    }
    
    // MARK: - IS DEALER LOGIN
    
    public static var isUserLoggedIn: Bool {
        get{
            let alreadyLogin = AppDefaults.defaults.bool(forKey: "isUserLoggedIn")
            return alreadyLogin
        }
        set{
            AppDefaults.defaults.set(newValue, forKey: "isUserLoggedIn")
            
        }
    }
    
    
//    //MARK: - interiorImageAngles
//    
//    public static var interiorImageAngles: [ImageAngleArray]? {
//        get{
//            if let data = AppDefaults.defaults.data(forKey: "interiorImageAngles") {
//                let decoder = JSONDecoder()
//                do{
//                    let decoded = try decoder.decode([ImageAngleArray].self, from: data)
//                    return decoded
//                }catch{
//                    return nil
//                }
//            }
//            return nil
//        }
//        set{
//            let encoder = JSONEncoder()
//            do {
//                let jsonData = try encoder.encode(newValue)
//                AppDefaults.defaults.set(jsonData, forKey: "interiorImageAngles")
//                
//            }catch{
//                print("interiorImageAngles not save")
//            }
//        }
//    }
    
}
