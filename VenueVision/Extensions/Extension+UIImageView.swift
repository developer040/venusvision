//
//  Extension+UIImageView.swift
//  VenueVision
//
//  Created by Muhammad Raza on 8/6/19.
//  Copyright © 2019 Muhammad Raza. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setOverlay(withColor color: UIColor) {
        if let image = self.image?.withRenderingMode(.alwaysTemplate) {
            self.image = image
            self.tintColor = color
        }
    }
}
